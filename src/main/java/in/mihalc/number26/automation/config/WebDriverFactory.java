package in.mihalc.number26.automation.config;

import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Created by Patrik Mihalčin
 */
public interface WebDriverFactory {

    RemoteWebDriver get(String url);
}
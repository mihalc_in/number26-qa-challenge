package in.mihalc.number26.automation.config;

import in.mihalc.number26.automation.exception.AutomationException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Patrik Mihalčin
 */
public class WebDriverFactoryImpl implements WebDriverFactory, ApplicationListener<ContextClosedEvent> {

    private static final Logger log = LoggerFactory.getLogger(WebDriverFactoryImpl.class);

    protected static final String DRIVER_REQUEST_TIMEOUT_SEC = "driver.request.timeout.sec";
    protected static final String CHROME_DRIVER_EXECUTABLE_PATH = "chrome.driver.executable.path";
    protected static final String CHROME_DRIVER_LOG_PATH = "chrome.driver.log.path";
    protected static final String MAX_ATTEMPTS_TO_TRY_BEFORE_THROWING_EX = "max.attempts.to.try.before.throwing.ex";

    private RemoteWebDriver driver;
    private Integer driverRequestTimeoutSec;
    private String chromeDriverExecutablePath;
    private String chromeDriverLogPath;
    private int maxAttemptsToTry;

    @Autowired
    private Environment environment;

    @PostConstruct
    private void initialize() {
        driverRequestTimeoutSec = environment.getProperty(DRIVER_REQUEST_TIMEOUT_SEC, Integer.class, 5);
        chromeDriverExecutablePath = environment.getProperty(CHROME_DRIVER_EXECUTABLE_PATH);
        chromeDriverLogPath = environment.getProperty(CHROME_DRIVER_LOG_PATH);
        maxAttemptsToTry = environment.getProperty(MAX_ATTEMPTS_TO_TRY_BEFORE_THROWING_EX, Integer.class, 3);
    }

    private ChromeDriverService createChromeDriverService() {
        if (chromeDriverExecutablePath == null) {
            throw new AutomationException(CHROME_DRIVER_EXECUTABLE_PATH + " property is not set.");
        }
        File chromeDriverFile = new File(chromeDriverExecutablePath);
        return new ChromeDriverService.Builder()
                .usingDriverExecutable(chromeDriverFile)
                .usingAnyFreePort()
                .withVerbose(true)
                .withLogFile(new File(chromeDriverLogPath))
                .build();
    }

    public RemoteWebDriver create() throws IOException {
        log.info("Starting chrome driver...");
        ChromeDriverService service = createChromeDriverService();
        ChromeDriver driver = new ChromeDriver(service);
        log.info("Chrome driver started");
        driver.manage().timeouts().implicitlyWait(driverRequestTimeoutSec, TimeUnit.SECONDS);
        return driver;
    }

    @Override
    public void onApplicationEvent(ContextClosedEvent contextClosedEvent) {
        if (driver != null) {
            log.info("Stopping chrome driver...");
            driver.quit();
            driver = null;
        }
    }

    @Override
    public RemoteWebDriver get(String url) {
        if (driver == null) {
            acquireNewDriver();
        }
        for (int i = 1; i <= maxAttemptsToTry; i++) {
            log.info("Getting URL (attempt {}/{}): {} ", i, maxAttemptsToTry, url);
            try {
                driver.get(url);
                try {
                    // if popup appears, just accept it
                    driver.switchTo().alert().accept();
                } catch (NoAlertPresentException ignored) {
                }
                return this.driver;
            } catch (TimeoutException e) {
                log.debug("Getting URL failed", e);
                releaseDriver();
                acquireNewDriver();
            }
        }
        throw new AutomationException("Unable to get URL. Exceeded maximum number of attempts to try (" + maxAttemptsToTry + ")");
    }

    private void acquireNewDriver() {
        if (driver != null) {
            throw new AutomationException("Existing driver must be released before acquiring new driver");
        }
        try {
            this.driver = create();
        } catch (IOException e) {
            throw new AutomationException("Error creating driver.", e);
        }
    }

    private void releaseDriver() {
        try {
            if (driver != null) {
                log.info("Releasing driver");
                driver.quit();
            }
        } catch (Exception e) {
            log.error("Error releasing driver.", e);
        } finally {
            driver = null;
        }
    }
}

package in.mihalc.number26.automation.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Patrik Mihalčin
 */
@Configuration
@ComponentScan(basePackages = {"in.mihalc.number26.automation.config"})
public class WebDriverConfiguration {

    @Bean
    public WebDriverFactory webDriverFactory() {
        return new WebDriverFactoryImpl();
    }

}

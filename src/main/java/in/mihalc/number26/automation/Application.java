package in.mihalc.number26.automation;

import in.mihalc.number26.automation.config.WebDriverConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * Created by Patrik Mihalčin
 */
@SpringBootApplication
@Import({WebDriverConfiguration.class})
public class Application {
    private static final Logger log = LoggerFactory.getLogger(Application.class);

    @PostConstruct
    public void bootUp() {
        log.info("Starting NUMBER26 Automation");
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

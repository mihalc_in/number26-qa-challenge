package in.mihalc.number26.automation.service;

import in.mihalc.number26.automation.config.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created by Patrik Mihalčin
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    protected WebDriverFactory webDriverFactory;

    @Override
    public boolean login(String email, String passwd) throws IOException {
        try {
            // hit number26 webapp and locate elements
            RemoteWebDriver driver = webDriverFactory.get("https://my.number26.de/");
            WebElement emailElem = driver.findElement(By.cssSelector("input[type=email]"));
            WebElement passwdElem = driver.findElement(By.cssSelector("input[type=password]"));
            WebElement loginBtnElem = driver.findElement(By.cssSelector("a.login"));

            // set inputs and submit
            emailElem.sendKeys(email);
            passwdElem.sendKeys(passwd);
            loginBtnElem.click();

            // check auth was successful by trying to find one of elements in main dashboard
            driver.findElement(By.cssSelector("div.holder.activities"));
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}

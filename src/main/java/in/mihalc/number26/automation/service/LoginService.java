package in.mihalc.number26.automation.service;

import java.io.IOException;

/**
 * Created by Patrik Mihalčin
 */
public interface LoginService {
    boolean login(String email, String passwd) throws IOException;
}

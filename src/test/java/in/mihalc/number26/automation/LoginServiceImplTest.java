package in.mihalc.number26.automation;

import in.mihalc.number26.automation.service.LoginService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.io.IOException;

/**
 * Created by Patrik Mihalčin
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class LoginServiceImplTest {

    private static final Logger log = LoggerFactory.getLogger(LoginServiceImplTest.class);

    @Autowired
    private LoginService loginService;

    @Test
    public void loginSuccessful() throws IOException {
        log.info("TEST loginSuccessful...");
        boolean wasSuccessful = loginService.login("mueller@mdesigns.at", "123asdF");
        Assert.assertTrue("Login wasn't successful", wasSuccessful);
    }

    @Test
    public void loginUnsuccessful() throws IOException {
        log.info("TEST loginUnsuccessful...");
        boolean wasSuccessful = loginService.login("test", "test");
        Assert.assertFalse("Login was successful", wasSuccessful);
    }

}

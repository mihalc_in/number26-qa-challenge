# NUMBER26 QA challenge #

### What is this repository for? ###
This repository contains solutions to 2 assignments as part of NUMBER26 QA challenge

1) Link to Trello board with high level test plan for the mobile app - https://trello.com/b/lhYjeBZx

Please note Trello board contains multiple lists and cards. Card can have details as illustrated below:

![trello-card.PNG](https://bitbucket.org/repo/46KEM8/images/3347245535-trello-card.PNG)


![trello-card-detail.PNG](https://bitbucket.org/repo/46KEM8/images/2074284097-trello-card-detail.PNG)


2) Short automated test for webapp login ([my.number26.de](my.number26.de)) to ensure that the login is working as expected. 
The solution is implemented in Java, built with [Gradle](http://gradle.org/) and based on Selenium driver. 
I developed the solution on Windows machine, but it would not be a big deal to extend it for Mac and Linux machines.

### How to run automated test ###

* It is required to have [Gradle](http://gradle.org/) & Java set up
* cd number26-qa-challenge/
* gradle test


### Who do I talk to? ###

* In case of any questions, feel free to reach out. I am more than open to any questions, suggestions, corrections
* [Patrik Mihalčin](http://mihalc.in/)